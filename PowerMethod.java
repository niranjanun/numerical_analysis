import java.lang.Math;
import java.util.Random;

// loss of precision is illustrated by type casting
public class PowerMethod {
    public static void main(String args[]) {
	int order = (int)Math.sqrt(new Float(args.length));
	float matrix[][] = new float[order][order];
	for(int i=0; i<order; i++)
	    for(int j=0; j<order; j++) {
		matrix[i][j] = new Float(args[order*i + j]);
	    }
	System.out.println("input matrix:");
	for(int i=0; i<order; i++) {
	    for(int j=0; j<order; j++) {
		System.out.print(matrix[i][j]+"  ");
	    }
	    System.out.println();
	}
	float eigenvector[] = new float[order];
	float eigenvectorPrevious[] = new float[order];
	System.out.println("initial guess for eigenvector:");
	for(int i=0; i<order; i++)
	    eigenvector[i] = (new Random()).nextFloat();
	float norm = 0;
        for(int i=0; i<order; i++)
	    norm += Math.pow(eigenvector[i], 2);
	for(int i=0; i<order; i++) {
	    eigenvector[i] /= (float)Math.sqrt(norm);
	    System.out.println(eigenvector[i]);
	}
	int iteration = 0;
	float convergence = 0;
	System.out.println("power method iterations:");
	System.out.println("(input matrix must be real, symmetric, square)");
	do {
	    eigenvectorPrevious = eigenvector;
	    convergence = 0;
	    norm = 0;
	    for(int i=0; i<order; i++) {
		eigenvector[i] = 0;
		for(int j=0; j<order; j++)
		    eigenvector[i] += matrix[i][j]*eigenvectorPrevious[j];
		norm += Math.pow(eigenvector[i], 2);
	    }
	    for(int i=0; i<order; i++) {
		eigenvector[i] /= Math.sqrt(norm);
		System.out.println(eigenvector[i]);
		convergence += Math.pow(eigenvector[i]-eigenvectorPrevious[i], 2);
	    }
	    System.out.println("iteration number = "+iteration++);
	    System.out.println("mean-squared error = "+convergence);
	} while(convergence > 0.0001);
    }
}
