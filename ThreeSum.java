import java.util.Arrays;

// O(n^2) for 3-sum problem
public class ThreeSum {
    public static void main(String args[]) {
	float input[] = new float[args.length];
	for(int i=0; i<args.length; i++) {
	    input[i] = (new Float(args[i])).floatValue();
	}
	Arrays.sort(input);
	for(int i=0; i<args.length; i++) {
	    float a = input[i];
	    int k = i+1, l = args.length-1;
	    while(k<l) {
		float b = input[k];
		float c = input[l];
		if(a+b+c == 0) {
		    System.out.println("3-sum search successful:");
		    System.out.format("a = %f, b = %f, c = %f\n", a, b, c);
		    System.exit(0);
		}
		else if(a+b+c > 0)
		    l -= 1;
		else
		    k += 1;
	    }
	}
    }
}
